package com.cardel.users.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class OBModel extends SugarRecord<OBModel> implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1566696297365976117L;

	protected String obId;
	
	@Expose
	@SerializedName("_identifier")
	protected String identifier;
	
	@Expose
	@SerializedName("_entityName")
	protected String entityName;
	@Expose
	@SerializedName("creationDate")
	protected String creationDate;
	
	public String getObId() {
		return obId;
	}
	public void setObId(String obId) {
		this.obId = obId;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((obId == null) ? 0 : obId.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OBModel other = (OBModel) obj;
		if (obId == null) {
			if (other.obId != null)
				return false;
		} else if (!obId.equals(other.obId))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return entityName+" [identifier=" + identifier + "]";
	}
}
