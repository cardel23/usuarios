package com.cardel.users.model;

import android.graphics.Bitmap;

import com.google.gson.JsonObject;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User extends OBModel implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6173517186857498311L;

	@Expose
	@SerializedName("email")
	private String email;

	@Expose
	@SerializedName("picture")
	private Pictures pictures;

	@Expose
	@SerializedName("name")
	private Name name;

	@Expose
	@SerializedName("registered")
	private String registerDate;

	@Expose
	@SerializedName("dob")
	private String birthday;

	@Expose
	@SerializedName("phone")
	private String phone;

	@Expose
	@SerializedName("cell")
	private String cellphone;

	private String bmp;

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (obId == null) {
			if (other.obId != null)
				return false;
		} else if (!obId.equals(other.obId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [name=" + getEmail() + "]";
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public Pictures getPictures() {
		return pictures;
	}

	public void setPictures(Pictures pictures) {
		this.pictures = pictures;
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public String getBmp() {
		return bmp;
	}

	public void setBmp(String bmp) {
		this.bmp = bmp;
	}
}
