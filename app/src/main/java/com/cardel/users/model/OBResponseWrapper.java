package com.cardel.users.model;

import com.google.gson.annotations.SerializedName;

public class OBResponseWrapper {
	@SerializedName("response")
	private Response response;
	
	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}


	public class Response{
		@SerializedName("status")
		private Integer status;
		@SerializedName("error")
		private Error error;
		@SerializedName("totalRows")
		private Integer totalRows;
		
		public Integer getStatus() {
			return status;
		}

		public void setStatus(Integer status) {
			this.status = status;
		}

		public Error getError() {
			return error;
		}

		public void setError(Error error) {
			this.error = error;
		}

		public Integer getTotalRows() {
			return totalRows;
		}

		public void setTotalRows(Integer totalRows) {
			this.totalRows = totalRows;
		}

		public class Error{
			@SerializedName("message")
			private String message;
			@SerializedName("messageType")
			private String messageType;
			@SerializedName("title")
			private String title;
			
			public String getMessage() {
				return message;
			}
			public void setMessage(String message) {
				this.message = message;
			}
			public String getMessageType() {
				return messageType;
			}
			public void setMessageType(String messageType) {
				this.messageType = messageType;
			}
			public String getTitle() {
				return title;
			}
			public void setTitle(String title) {
				this.title = title;
			}			
		}
	}
	
}

