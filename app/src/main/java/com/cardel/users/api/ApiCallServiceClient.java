package com.cardel.users.api;

import android.content.Context;
import android.util.Log;

import com.cardel.users.model.User;
import com.cardel.users.util.UsersApp;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by pwk04 on 05-17-17.
 */

public class ApiCallServiceClient {

    private static UsersApp app;
    private static ApiCallService apiCallService;
    private static String serviceURL;

    public ApiCallServiceClient(Context context){
        app = (UsersApp) context;
        StringBuilder serviceUrlBuilder = new StringBuilder();
        serviceUrlBuilder.append("https://randomuser.me")
//        serviceUrlBuilder.append("http://192.168.31.102:12769/")
//        serviceUrlBuilder.append("http://192.168.0.13:8080/")
//                .append(ApiCallService.REST_BASE_URL)
        ;
        serviceURL = serviceUrlBuilder.toString();
    }

    public static ApiCallService initializeService(){
        try {
            OkHttpClient httpClient = new OkHttpClient();
            httpClient.setReadTimeout(40, TimeUnit.SECONDS);
            httpClient.setConnectTimeout(40, TimeUnit.SECONDS);
            Gson gson = new GsonBuilder().setFieldNamingPolicy(
                    FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                    .registerTypeAdapter(List.class, new OBListDeserializer<User>())
                    .excludeFieldsWithoutExposeAnnotation()
                    .create();

            httpClient.interceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    try{
                        Request request = chain.request();
                        Log.d("Request", request.urlString());
                        Request newRequest = request.newBuilder()
                                .header("Accept", "application/json")
                                .build();
                        Response originalResponse = chain.proceed(newRequest);
                        return originalResponse;
                    }
                    catch (IOException e){
                        return null;
                    }
                }
            });
//            }
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(getURL())
                    .client(httpClient)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson));
            Retrofit adapter = builder.build();
            apiCallService = adapter.create(ApiCallService.class);
            return apiCallService;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static String getURL(){
        return serviceURL;
    }

}
