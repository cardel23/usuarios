package com.cardel.users.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by pwk04 on 05-17-17.
 */

public class Serializer<T> implements JsonSerializer<T> {

    private Class<T> typeParameterClass;
    private List<String> propertyNames;
    private Boolean wrapInData = true;

    public Serializer(Class<T> typeParameterClass, String[] propertyNames){
        this.typeParameterClass = typeParameterClass;
        if(propertyNames != null) {
            this.propertyNames = new ArrayList<>(Arrays.asList(propertyNames));
        }
    }

    public Serializer(Class<T> typeParameterClass, String[] propertyNames, Boolean wrapInData){
        this.typeParameterClass = typeParameterClass;
        if(propertyNames != null) {
            this.propertyNames = new ArrayList<>(Arrays.asList(propertyNames));
        }
        this.wrapInData = wrapInData;
    }

    @Override
    public JsonElement serialize(T src, Type typeOfSrc, JsonSerializationContext context) {
        Gson gson =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        JsonParser parser = new JsonParser();

        JsonObject jsonObject = parser.parse(gson.toJson(src, typeParameterClass)).getAsJsonObject();
        if(propertyNames != null){
            for(String propName : propertyNames){
                jsonObject.add(propName,
                        parser.parse("{\"id\": '" + jsonObject.get(propName).getAsString() + "'}"));
            }
        }
        if(wrapInData) {
            JsonObject objectWrapper = new JsonObject();
            objectWrapper.add("result", jsonObject);
            return objectWrapper;
        } else {
            return jsonObject;
        }
    }
}
