package com.cardel.users.api;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class OBListDeserializer<T> implements JsonDeserializer<List<T>> {
	public static final String TAG = "OBListDeserializer";
	public Class<T> classType;
	public List<T> deserialize(JsonElement json, Type type,
							   JsonDeserializationContext context) throws JsonParseException {
			Gson gson = new GsonBuilder().
					excludeFieldsWithoutExposeAnnotation()
					.create();
		try {
			JsonObject jsonResponse = new JsonParser().parse(json.toString()).getAsJsonObject();
			if (jsonResponse.has("results")) {
				JsonArray responseArray = jsonResponse.getAsJsonArray("results");
				List<T> elementsArray;

				try {
					elementsArray = gson.fromJson(responseArray.toString(), type);
				} catch (Exception e) {
                    elementsArray = new ArrayList<>();
                    try {

                        for (JsonElement jsonElement : responseArray
                                ) {
                            T element = context.deserialize(jsonElement, Class.forName("com.cardel.users.model.User"));
                            elementsArray.add(element);
                        }
                    } catch (JsonParseException e12){
                        throw e12;

                    } catch (ClassNotFoundException e1) {
                        e1.printStackTrace();
                    }
                    throw new JsonParseException(e.getMessage());
				}
				return elementsArray;
			}
		} catch (JsonParseException e) {
			Log.e(TAG, e.getMessage(), e);
			throw e;
		}				
		return null;
	}
}
