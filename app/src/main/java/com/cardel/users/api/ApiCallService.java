package com.cardel.users.api;

import com.cardel.users.model.User;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by carlos on 03-23-18.
 */

public interface ApiCallService {

    @GET("/api?results=10")
    public Call<List<User>> getUserList();

}
