package com.cardel.users.event;

import java.util.List;

public interface OnPostExecuteListener<T> {
    public void onPostExecute(T response, List<String> errorMessages);
    public void onPostExecute(T response);
}
