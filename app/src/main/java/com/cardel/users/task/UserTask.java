package com.cardel.users.task;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.cardel.users.api.ApiCallService;
import com.cardel.users.api.ApiCallServiceClient;
import com.cardel.users.event.OnPostExecuteListener;
import com.cardel.users.model.User;
import com.cardel.users.util.UsersApp;
import com.cardel.users.util.Utils;

import java.net.URL;
import java.util.List;

/**
 * Created by carlos on 03-23-18.
 */

public class UserTask extends TaskWithCallback<Void,Void,List<User>> {

    private OnPostExecuteListener<List<User>> callback;
    private String id;
    private retrofit.Response response;

    public UserTask(Context context) {
        super(context);
    }

    @Override
    protected List<User> doInBackground(Void... voids) {
        ApiCallService service = new ApiCallServiceClient(app).initializeService();
        List<User> list;
        try {
            response = service.getUserList().execute();
            list = (List<User>) response.body();
            for (User user: list) {
                URL url = new URL(user.getPictures().getLarge());
                Bitmap btp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                user.setBmp(Utils.BitMapToString(btp));
                user.getName().save();
                user.getPictures().save();
                user.save();

            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<User> users) {
        super.onPostExecute(users);
        this.callback.onPostExecute(users);
    }

    public OnPostExecuteListener<List<User>> getCallback() {
        return callback;
    }

    public void setCallback(OnPostExecuteListener<List<User>> callback) {
        this.callback = callback;
    }

}
