package com.cardel.users.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.cardel.users.api.ApiCallService;
import com.cardel.users.model.User;
import com.orm.SugarApp;

import java.io.ByteArrayOutputStream;
import java.util.List;

/**
 * Created by carlos on 03-23-18.
 */

public class UsersApp extends SugarApp {
    private ApiCallService service;
    private List<User> currentUsers;
    private boolean start = false;
    public ApiCallService getService() {
        return service;
    }

    public void setService(ApiCallService service) {
        this.service = service;
    }

    public List<User> getCurrentUsers() {
        if(currentUsers == null)
            currentUsers = User.listAll(User.class);
        return currentUsers;
    }


    @Override
    public void onCreate() {
        super.onCreate();
    }

    public boolean isStart() {
        return start;
    }

    public void setStart(boolean start) {
        this.start = start;
    }



}
