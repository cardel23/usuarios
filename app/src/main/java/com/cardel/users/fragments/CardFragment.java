package com.cardel.users.fragments;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cardel.users.R;
import com.cardel.users.adapter.UserAdapter;
import com.cardel.users.adapter.UserViewHolder;
import com.cardel.users.api.ApiCallServiceClient;
import com.cardel.users.event.OnPostExecuteListener;
import com.cardel.users.model.User;
import com.cardel.users.task.UserTask;
import com.cardel.users.util.UsersApp;

import java.util.ArrayList;
import java.util.List;


public class CardFragment extends Fragment {

    List<User> listitems;
    RecyclerView MyRecyclerView;
    UsersApp app;
    View mainView;
    UserTask task;
    LinearLayoutManager MyLayoutManager;
    public CardFragment (){}
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (UsersApp) getActivity().getApplicationContext();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_card, container, false);
        MyRecyclerView = (RecyclerView) mainView.findViewById(R.id.cardView);
        MyRecyclerView.setHasFixedSize(true);
        MyLayoutManager= new LinearLayoutManager(getActivity());
        MyLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        return mainView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(!app.isStart()){
            task = new UserTask(getActivity());
            task.setCallback(new OnPostExecuteListener<List<User>>() {
                @Override
                public void onPostExecute(List<User> response, List<String> errorMessages) {

                }

                @Override
                public void onPostExecute(List<User> response) {
                    listitems = app.getCurrentUsers();
                    app.setStart(true);
                    createView(listitems);
                }
            });
            task.execute();
        }
        else {
            listitems = app.getCurrentUsers();
            createView(listitems);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void createView(List<User> listitems) {
        if (listitems.size() > 0 & MyRecyclerView != null) {
            MyRecyclerView.setAdapter(new UserAdapter(listitems));
        }
        MyRecyclerView.setLayoutManager(MyLayoutManager);
    }

}