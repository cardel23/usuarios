package com.cardel.users.adapter;

import android.content.ContentResolver;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cardel.users.R;

/**
 * Created by carlos on 03-23-18.
 */

public class UserViewHolder extends RecyclerView.ViewHolder {
    CardView cv;
    TextView personName;
    TextView personAge;
    TextView personEmail;
    ImageView personPhoto;

    public UserViewHolder(View itemView) {
        super(itemView);
        cv = (CardView)itemView.findViewById(R.id.cv);
        personName = (TextView)itemView.findViewById(R.id.person_name);
        personAge = (TextView)itemView.findViewById(R.id.person_age);
        personEmail = (TextView)itemView.findViewById(R.id.person_email);
        personPhoto = (ImageView)itemView.findViewById(R.id.person_photo);
    }
}

