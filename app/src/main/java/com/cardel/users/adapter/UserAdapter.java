package com.cardel.users.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cardel.users.R;
import com.cardel.users.model.User;
import com.cardel.users.util.Utils;

import org.json.JSONException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.R.id.list;

/**
 * Created by carlos on 03-23-18.
 */

public class UserAdapter extends RecyclerView.Adapter<UserViewHolder> {
    private List<User> users;

    public UserAdapter(List<User> Data) {
        users = Data;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recicle_users, parent, false);
        UserViewHolder holder = new UserViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final UserViewHolder holder,  int i) {
        try {
            holder.personName.setText(users.get(i).getName().getFirst());
            holder.personAge.setText(users.get(i).getBirthday());
            holder.personEmail.setText(users.get(i).getEmail());
            holder.personPhoto.setImageBitmap(Utils.StringToBitMap(users.get(i).getBmp()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}
